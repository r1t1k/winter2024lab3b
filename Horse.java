public class Horse{
	public String breed;
	public int weight;
	public int speed;
	
	public void canRun(){
		if(this.speed > 25){
			System.out.println("This " + this.breed + " can run very fast for its breed.");
		}
		else{
			System.out.println("This " + this.breed + " can run at an average speed for its age.");
		}
	}
	public void eatFood(){
		if(this.weight < 368 ){
			System.out.println("This " + this.breed + " was " + this.weight + ". Now after eating 7kg of hay it has gained weight, and now weighs " + (this.weight + 3) + "kg");
			this.weight += 3;
		}
		else{
			System.out.println("The " + this.breed + " is " + (this.weight - 360) + "kg over the breed's average weight and needs to loose weight so there is no food for it.");
		}		
	}
}