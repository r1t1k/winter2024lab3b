import java.util.Scanner;
public class VirtualPetApp{
	public static void main(String[] args){
		Scanner reader = new Scanner(System.in);
		Horse[] stableOfHorses = new Horse[4];
		for(int i = 0; i < stableOfHorses.length; i++){
			stableOfHorses[i] = new Horse();
			System.out.println("What is the breed of your Horse: ");
			stableOfHorses[i].breed = reader.nextLine();
			System.out.println("What is your Horse's weight: ");
			stableOfHorses[i].weight = Integer.parseInt(reader.nextLine());
			System.out.println("How FAST is your Horse: ");
			stableOfHorses[i].speed = Integer.parseInt(reader.nextLine());
		}
		System.out.println(stableOfHorses[3].breed);
		System.out.println(stableOfHorses[3].weight);
		System.out.println(stableOfHorses[3].speed);
		stableOfHorses[0].eatFood();
		stableOfHorses[0].canRun();
	}
}